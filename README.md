# Installation

## With virtualenv
```
git clone git@github.com:ultralytics/yolov5.git
cd yolov5
git checkout 886f1c0
wget https://github.com/ultralytics/yolov5/releases/download/v4.0/yolov5l.pt

cd ..
virtualenv -p python3 venv
source venv/bin/activate
pip install -r yolov5/requirements.txt
pip install jupyterlab sklearn tqdm==4.61.2

jupyter lab
```

## With Docker and docker-compose
1. Install `docker`
3. Install `docker-compose`
2. Install `nvidia-container-runtime`  https://docs.docker.com/config/containers/resource_constraints/#gpu
4. Run `docker-compose`  
    ```
    docker-compose up
    ```


# Running experiment
1. Download dataset from trapper and put the `observations.csv` file in the `code/trapperdata` directory.
2. All data preparation and the experiment is in `Run experiment - CV.ipynb` notebook.  
    In the virtualenv setup its in the `code/Run experiment - CV.ipynb`
3. If you don't need to change anything, simply run all cells.


# wandb
Optionally you can use wandb for logging experiments.
To do that install `wandb` package. In the Docker setup you can uncomment the following line in the `Docker` file
```
# RUN pip install wandb
```
 and rebuild the docker.
```
docker-compose build
```

Next login to the `wandb` by running `wandb login` from the command line. If you are using the Docker setup then run this command inside the container.
