FROM pytorch/pytorch:1.7.1-cuda11.0-cudnn8-devel
ENV PYTHONUNBUFFERED 1
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt-get update \
    && apt-get install -y \
        git \
        libgl1-mesa-dev \
        libglib2.0-0 \
        libsm6 \
        libxrender1 \
        libxext6 \
        wget

WORKDIR /

## SETUP user
RUN useradd -ms /bin/bash docker-user

# CLONE YOLOV5
RUN git clone https://github.com/ultralytics/yolov5.git

WORKDIR /yolov5
RUN git checkout 886f1c0

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install jupyterlab sklearn tqdm==4.61.2
# RUN pip install wandb

RUN wget https://github.com/ultralytics/yolov5/releases/download/v4.0/yolov5l.pt
# RUN ./weights/download_weights.sh

RUN mkdir /code
WORKDIR /code

COPY ./code /code
RUN chown -R docker-user:docker-user /code
RUN chown -R docker-user:docker-user /yolov5

USER docker-user